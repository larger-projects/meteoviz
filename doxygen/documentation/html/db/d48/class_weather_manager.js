var class_weather_manager =
[
    [ "WeatherManager", "db/d48/class_weather_manager.html#a8bde95e66ead10b61d9c538617dcd42e", null ],
    [ "callFourDayForecast", "db/d48/class_weather_manager.html#a1fd3fb7080ec307bef7512c36d380124", null ],
    [ "callGraphForecast", "db/d48/class_weather_manager.html#adfe35052db817f5cfbbeb153c99e80e5", null ],
    [ "refreshRequest", "db/d48/class_weather_manager.html#a92382e09a123350a940bc5c8c9f34277", null ],
    [ "sendCity", "db/d48/class_weather_manager.html#a99eec32a2e71740133864052da9e8a1d", null ],
    [ "sendFourDayForecast", "db/d48/class_weather_manager.html#a2d914bd8cda5ef252ac60377292fc279", null ],
    [ "sendGraphForecast", "db/d48/class_weather_manager.html#aeaf426486dfaa4d71d77bfb1f45a71b3", null ],
    [ "setCityLabel", "db/d48/class_weather_manager.html#a147860fa8f36c26bdfa118d3c4e012db", null ],
    [ "setFourDayForecast", "db/d48/class_weather_manager.html#aee4918e40bb2327e3cd6db6a1c7bda41", null ],
    [ "setGraphForecast", "db/d48/class_weather_manager.html#a0745d2a557a37d57f1fb7704f042c1b1", null ]
];