var class_url_connection =
[
    [ "UrlConnection", "d1/dd6/class_url_connection.html#a3991c180b1b29c5a59280eb8ca61af17", null ],
    [ "UrlConnection", "d1/dd6/class_url_connection.html#a7b5558c6d78e96db5b6b217884d6810b", null ],
    [ "callDailyWeather", "d1/dd6/class_url_connection.html#aa4dd1a83db6b6505f6beba414c381133", null ],
    [ "callGeneralWeather", "d1/dd6/class_url_connection.html#ab482ca4bfdfb349b43b6a4a61d6c6a34", null ],
    [ "callGraphWeather", "d1/dd6/class_url_connection.html#aaaace9190222c93ca08fff73f0c0163d", null ],
    [ "fromStringToIntervals", "d1/dd6/class_url_connection.html#a186e5d1c2be57f3c0d44622204aa4b59", null ],
    [ "getApiKey", "d1/dd6/class_url_connection.html#a9da850d90136af15b7c558e6afe91251", null ],
    [ "readFourDayForecast", "d1/dd6/class_url_connection.html#aa2040c08a494cb7e8cdfe87cf58c6fc5", null ],
    [ "readGraphWeather", "d1/dd6/class_url_connection.html#af75bee2f88a1ecd37e64d35cdf3a9b0e", null ],
    [ "readWeathercode", "d1/dd6/class_url_connection.html#a20fa22c73b6d32edc9b5062853ba03f8", null ]
];