var struct_daily_forecast =
[
    [ "humidity", "da/da4/struct_daily_forecast.html#ae84bfb7aad858b22ac440f5689cd6746", null ],
    [ "precipitation", "da/da4/struct_daily_forecast.html#a2910daa87e625f5a7a35be5ce99066c0", null ],
    [ "pressure", "da/da4/struct_daily_forecast.html#aabf8d6a2c302de1506f836dc451f31c9", null ],
    [ "temperature", "da/da4/struct_daily_forecast.html#ab92a053dad2d276e0510f98e2a4fc91a", null ],
    [ "weatherCode", "da/da4/struct_daily_forecast.html#a40ff11a4a91755c3f129c12cc070944f", null ],
    [ "windDirection", "da/da4/struct_daily_forecast.html#a7727090b1209010ca3e0c67f93b9ffe0", null ],
    [ "windSpeed", "da/da4/struct_daily_forecast.html#a038582adfdf91d29128ebd043fb9a983", null ]
];