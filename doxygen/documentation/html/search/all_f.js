var searchData=
[
  ['weathercode_45',['weatherCode',['../d5/d78/struct_coords_weather_data.html#a4eb65a12ddbffed7586a55f5aa0642ae',1,'CoordsWeatherData::weatherCode()'],['../da/da4/struct_daily_forecast.html#a40ff11a4a91755c3f129c12cc070944f',1,'DailyForecast::weatherCode()']]],
  ['weathermanager_46',['WeatherManager',['../db/d48/class_weather_manager.html',1,'WeatherManager'],['../db/d48/class_weather_manager.html#a8bde95e66ead10b61d9c538617dcd42e',1,'WeatherManager::WeatherManager()']]],
  ['winddirection_47',['windDirection',['../da/da4/struct_daily_forecast.html#a7727090b1209010ca3e0c67f93b9ffe0',1,'DailyForecast']]],
  ['windspeed_48',['windSpeed',['../da/da4/struct_daily_forecast.html#a038582adfdf91d29128ebd043fb9a983',1,'DailyForecast']]]
];
