var annotated_dup =
[
    [ "AppManager", "da/d96/class_app_manager.html", "da/d96/class_app_manager" ],
    [ "CitychoiceManager", "d5/d3e/class_citychoice_manager.html", "d5/d3e/class_citychoice_manager" ],
    [ "CityCoords", "d3/d7c/struct_city_coords.html", "d3/d7c/struct_city_coords" ],
    [ "CoordsWeatherData", "d5/d78/struct_coords_weather_data.html", "d5/d78/struct_coords_weather_data" ],
    [ "DailyForecast", "da/da4/struct_daily_forecast.html", "da/da4/struct_daily_forecast" ],
    [ "GraphData", "de/d2e/struct_graph_data.html", "de/d2e/struct_graph_data" ],
    [ "UrlConnection", "d1/dd6/class_url_connection.html", "d1/dd6/class_url_connection" ],
    [ "WeatherManager", "db/d48/class_weather_manager.html", "db/d48/class_weather_manager" ]
];