var hierarchy =
[
    [ "CityCoords", "d3/d7c/struct_city_coords.html", null ],
    [ "CoordsWeatherData", "d5/d78/struct_coords_weather_data.html", null ],
    [ "DailyForecast", "da/da4/struct_daily_forecast.html", null ],
    [ "GraphData", "de/d2e/struct_graph_data.html", null ],
    [ "QObject", null, [
      [ "AppManager", "da/d96/class_app_manager.html", null ],
      [ "CitychoiceManager", "d5/d3e/class_citychoice_manager.html", null ],
      [ "WeatherManager", "db/d48/class_weather_manager.html", null ]
    ] ],
    [ "UrlConnection", "d1/dd6/class_url_connection.html", null ]
];