#ifndef CITYCHOICEMANAGER_HH
#define CITYCHOICEMANAGER_HH

#include <QObject>
#include <QVariant>
#include <QDebug>
#include <QMessageBox>
#include <QMap>
#include "url_connection.hh"

/*! Class responsible for managing the first window (the one enabling the choice of the city) including initializing the cities list, setting location pin position, evoking weather call for weather icons on the map etc.
 */
class CitychoiceManager : public QObject
{
    Q_OBJECT

    private:
        UrlConnection _urlCon;                              /**< object enabling URL connection to the weather API */
        QPair<QString, CityCoords> _selectedCity;           /**< pair of the selected location name and its geocoordinates */
        QList <QVariant> _availableCities;                  /**< list of the names of all available cities */
        QMap <QString, CityCoords> _cities;                 /**< map of pairs city_name-geocoords of all available cities */
        QMap <QString, CoordsWeatherData> _generalCities;   /**< map of pairs city_name-geocoords_with_weather of chosen locations for which the weather on the map is displayed */

    public:
        /*!
	 * @brief Public constructor
	 * @param[in] allCitiesFilepath - string being the path to the file with all cities (names and coordinates)
         * @param[in] generalCitiesFilepath - string being the path to the file with chosen locations (names and coordinates) for general weather displayed on the map
	 * @param[in] urlCon - object with set connection to weather API
         * @param[in] parent - pointer to the QObject parent
         */
        explicit CitychoiceManager(QString allCitiesFilepath, QString generalCitiesFilepath, UrlConnection urlCon, QObject *parent = nullptr);
        /*!
         * @brief Initialization of all cities - converting the data from file to _cities QMap
         * @param[in] fileName - string being the path to the file with all cities (names and coordinates)
         */
        void initCities(QString fileName);
        /*!
         * @brief Initialization of chosen cities with weather displayed on the map - converting the data from file to _generalCities QMap
         * @param[in] fileName - string being the path to the file with the cities (names and coordinates)
         */
        void initGeneralCities(QString fileName);
        /*!
         * @brief Initialization of the combobox (list) with all city names in frontend part
         */
        void initCitiesCombobox();
        /*!
         * @brief Initialization of the markers with general weather icons for chosen locations
         */
        void initMapItems();
        /*!
         * @brief Getting the information on the selected city (its name and geocoordinates)
         * @return pair of the selected city name and its geocoordinates
         */
        QPair<QString, CityCoords> selectedCity();

    signals:
        /*!
         * @brief Signal sending the list of all city names to QML, later injected into combobox object
         * @param[in] list - list of all available cities' names
         */
        void setCitiesCombobox(QVariant list);
        /*!
         * @brief Signal sending coordinates of the pin of the selected city
         * @param[in] latitude - geographical city latitude
         * @param[in] longitude - geographical city longitidue
         */
        void sendPinPosition(QVariant latitude, QVariant longitude);
        /*!
         * @brief Signal sending the list with geocoordinates and associated weather code of chosen locations for displaying general icons on the map
         * @param[in] list - list of [latitude, longitude, weatherCode] for chosen locations
         */
        void setMapItems(QVariant list);

    public slots:
        /*!
         * @brief Slot reacting to the selection of the city (sends its coordinates)
         * @param[in] city - selected city
         */
        void onCityChosen(QString city);
        /*!
         * @brief Slot saving the coordinates of a custom selection of a place on the map as the selected city 'name'
         * @param[in] coords - string with both coordinates separated with ','
         */
        void onCustomCoords(QString coords);
};

#endif // CITYCHOICEMANAGER_HH
