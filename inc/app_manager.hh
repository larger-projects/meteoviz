#ifndef APPMANAGER_HH
#define APPMANAGER_HH

#include <QObject>
#include <QQmlApplicationEngine>
#include <QApplication>
#include <QQuickWindow>
#include "citychoice_manager.hh"
#include "weather_manager.hh"



class QQmlApplicationEngine;
/*! Class responsible for managing the app, switching between windows,
 *  connecting slots with signals etc. It inherits from QObject. The class
 *  fields are pointers to both window managers, which allows the app manager
 *  to hide and show windows according to user's actions. 
 */
class AppManager : public QObject
{
    Q_OBJECT

    private:
        QQmlApplicationEngine *_engine = nullptr;       /**< Pointer to the QML application engine running the app */
        CitychoiceManager *_citychoiceMngr = nullptr;   /**< Pointer to the manager of the first window (the one with city choice option) */
        WeatherManager *_weatherMngr = nullptr;         /**< Pointer to the manager of the second window (the one with weather forecast) */
        bool _main = true;                              /**< Bool value stating if the first window is active or not */
        QQuickWindow *_window  = nullptr;               /**< Pointer to the currently active window */

    public:
        /**
	 * @brief Public constructor 
	 * @param[in] engine - pointer to the QML application engine running the app
         * @param[in] citychoiceMngr - pointer to the manager of the first window (the one with city choice option)
         * @param[in] weatherMngr - pointer to the manager of the second window (the one with weather forecast)
         * @param[in] parent - pointer to the QObject parent
         */
        explicit AppManager(QQmlApplicationEngine *engine, CitychoiceManager *citychoiceMngr,
                            WeatherManager *weatherMngr, QObject *parent = nullptr);
        /**
         * @brief Public method enabling switching between windows. It is checked which window is active, it is disactivated and the other window is enabled for the user. The method is invokable as it is evoked in QML part of the code.
         */
        Q_INVOKABLE void changeWindow();
        /*!
         * @brief Public method for initializing the slot-signal pairs regarding the first window (the one with city choice option)
         * @param[in] qObjectWindow - pointer to the chosen window (it always should be the first window) used for indicating the app context when connecting the slots with signals
         */
        void initCitychoiceConnections(QObject *qObjectWindow);
        /*!
         * @brief Public method for initializing the slot-signal pairs regarding the second window (the one with weather forecast)
         * @param[in] qObjectWindow - pointer to the chosen window (it always should be the second window) used for indicating the app context when connecting the slots with signals         
	 */
        void initWeatherConnections(QObject *qObjectWindow);

};

#endif // APPMANAGER_HH

