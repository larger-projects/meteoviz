#ifndef WEATHERMANAGER_HH
#define WEATHERMANAGER_HH

#include <QObject>
#include <QVariant>
#include <QMap>
#include "additional_structs.hh"
#include "url_connection.hh"


/*! Class responsible for managing the second window (the one displaying the weather forecast)
 *  including calling proper requests to weather API, sending received data to frontend 
 *  and refreshing the window. 
 */
class WeatherManager : public QObject
{
    Q_OBJECT

    private:
        QPair<QString, CityCoords> _selectedCity;       /**< pair of the selected city name and its geocoordinates */
        QMap<QString, DailyForecast> _fourDayWeather;   /**< map of pairs city_name-weather_data of the selected city
                                                             for the current day and three future days */
        QMap<QString, GraphData> _graphForecast;        /**< map of pairs city_name-graph_data of the selected city
                                                             for closest hours (precipitation and temperature)*/
        UrlConnection _urlCon;                          /**< object enabling URL connection with the weather API */


    public:
        /*!
         * @brief Public constructor
         * @param[in] urlCon - object with set connection to weather API
         * @param[in] parent - pointer to the QObject parent
         */
        explicit WeatherManager(UrlConnection urlCon, QObject *parent = nullptr);
        /*!
         * @brief Sending the string of city name or the coordinates to QML frontend
         * @param[in] city - pair of city_name-geocoordinates to be converted to the label
         *                  (in case a custom location is chosen, the label will contain the coordinates)
         */
        void sendCity(QPair<QString, CityCoords> city);
        /*!
         * @brief Evoking the URL request for four day forecast (today + 3) and saving the data to _fourDayWeather
         */
        void callFourDayForecast();
        /*!
         * @brief Converting the four day forecast data to QVariant and emitting a signal to QML
         */
        void sendFourDayForecast();
        /*!
         * @brief Evoking the URL request for hourly graph data and saving it to _graphForecast
         */
        void callGraphForecast();
        /*!
         * @brief Converting the graph forecast data to QVariants and emitting a signal to QML
         */
        void sendGraphForecast();

    signals:
        /*!
         * @brief Signal sending the city label to display in the second window
         * @param[in] cityName - the name (or coordinates) of the selected city
         */
        void setCityLabel(QVariant cityName);
        /*!
         * @brief Signal sending the four day forecast data to the QML frontend
         * @param[in] list - list with [temperature, pressure, windSpeed, windDirection,
         *              precitipation, humidity, weatherCode] data for each of the four days
         */
        void setFourDayForecast(QVariant list);
        /*!
         * @brief Signal sending the 8-hour temperature and precitipation
         *        forecast data to the QML frontend
         * @param[in] listTemp - list with temperature measurement forecast for 8 hours
         * @param[in] listPrec - list with precitipation measurement forecast for 8 hours
         */
        void setGraphForecast(QVariant listTemp, QVariant listPrec);

    public slots:
        /*!
         * @brief Slot for managing the refresh request
         */
        void refreshRequest();
};

#endif // WEATHERMANAGER_HH






