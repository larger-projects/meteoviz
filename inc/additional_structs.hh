#ifndef ADDITIONAL_STRUCTS_HH
#define ADDITIONAL_STRUCTS_HH

#include <QString>
#include <QVector>

/*! Struct for keeping the geocoordinations and the weather associated
 *  with them for a specific location. Coordinates (geographic latitude and
 *  longitude as double) and weather code indicating the type of current weather 
 *  (as integer) are necessary to describe the place marked with the proper 
 *  weather icon on the map.
 */
struct CoordsWeatherData
{
    double latitude  = 200.0; /**< Geograhic latitude coordinate */
    double longitude = 200.0; /**< Geograhic longitude coordinate */
    int weatherCode  = 0;     /**< Value corresponding to the current weather type */
};

/*! Struct for keeping the geocoordinations of a city. Two double values describe
 * geographic latitude and longitude coordinates of a specific location.
 */
struct CityCoords
{
    double latitude  = 200.0; /**< Geograhic latitude coordinate */
    double longitude = 200.0; /**< Geograhic longitude coordinate */
};

/*! Struct for keeping the daily weather data. The information consists
 *  of temperature, air pressure, wind speed, wind direction, precipitation
 *  intensity, humidity and weather type.
 */
struct DailyForecast
{
    int temperature = 0;        /**< Temperature value in degrees of Celcius */
    int pressure = 0;           /**< Atmospheric pressure value in hPa */
    int windSpeed = 0;          /**< Wind speed value in m/s */
    QString windDirection = ""; /**< Wind direction (NE, SE etc.) */
    int precipitation = 0;      /**< Intensity of rain in mm/h */
    int humidity = 0;           /**< Humidity in % */
    int weatherCode = 0;        /**< Value corresponding to the current weather type */
};


/*! Struct for keeping the data later plotted on graphs. Consists of a QVector with 
 *  subsequent measurements (of either precipitation intensity or temperature) in a
 *  specific time range.
 */
struct GraphData
{
    QVector<double> data;       /**< Subsequent measurements. */
};

#endif // ADDITIONAL_STRUCTS_HH
