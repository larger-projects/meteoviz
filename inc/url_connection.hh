#ifndef URL_CONNECTION_HH
#define URL_CONNECTION_HH

#include <QObject>
#include <QtNetwork>
#include <QDateTime>
#include <QtDebug>
#include <QMessageBox>
#include "additional_structs.hh"


/*! Class responsible for managing the requests to weather API - it creates GET calls to the server in accordance to user needs and processes the data from JSON to proper structs
 */
class UrlConnection
{
    QString _urlAddress = "";       /**< address used for URL requests */
    QString _apiKeyFilepath = "";   /**< path to the file with API key */

    public:
        /*!
         * @brief Non-parameter constructor
         */
        UrlConnection();
        /*!
         * @brief Constructor with parameters
         * @param[in] address - the URL address used for requests
         * @param[in] keyFilePath - the path to the file with API key
         */
        UrlConnection(QString address, QString keyFilePath);
        /*!
         * @brief Method retrieving the API key from the file to whom the path in _apiKeyFilepath is given
         */
        QString getApiKey();
        /*!
         * @brief Converting a string to JSON Array
         * @param[in] content - string with JSON-styled data
         * @return JSON Array with weather data intervals
         */
        QJsonArray fromStringToIntervals(QString content);
        /*!
         * @brief Calling a request on general weather for chosen locations displayed on the map
         * @param[out] generalWeatherMap - a reference to the map of pairs city_name-coordinates_with_weathercode to which we want to save the received data
         */
        void callGeneralWeather(QMap<QString, CoordsWeatherData> & generalWeatherMap);
        /*!
         * @brief Calling a request on daily weather for the current day and three subsequent days
         * @param[in] city - coordinates of the selected city
         * @param[out] fourDayForecast - a reference to the map with four pairs day-weather_data to which we want to save the received data
         */
        void callDailyWeather(CityCoords city, QMap<QString, DailyForecast> & fourDayForecast);
        /*!
         * @brief Calling a request on today's weather for interval of a couple of future hours (for graphs)
         * @param[in] city - coordinates of the selected city
         * @param[out] graphData - a reference to the map with four pairs day-graph_data to which we want to save the received data
         */
        void callGraphWeather(CityCoords city, QMap<QString, GraphData> & graphData);
        /*!
         * @brief Converting JSON-styled data to weather code
         * @param[in] content - string with JSON-styled data
         * @return The weather code (corresponding to the weather type)
         */
        int readWeathercode(QString content);
        /*!
         * @brief Converting JSON-styled data to daily weather data 
         * @param[in] content - string with JSON-styled data
         * @param[out] fourDayForecast - a reference to the map with four pairs day-weather_data to which the data from JSON should be written
         */
        void readFourDayForecast(QString content, QMap<QString, DailyForecast> & fourDayForecast);
        /*!
         * @brief Converting JSON-styled data to graph data (subsequent measurements for temperature and precipitation)
         * @param[in] content - string with JSON-styled data
         * @param[out] graphData - a reference to the map with four pairs day-graph_data to which the data from JSON should be written
         */
        void readGraphWeather(QString content, QMap<QString, GraphData> & graphData);
};

#endif // URL_CONNECTION_HH
