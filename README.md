MeteoViz - weather forecast visualization		{#mainpage}
==========================================


This repository contains the Qt-powered weather forecast desktop application. Backend of the app is implemented with C++ and Qt library, whereas the frontend part is developed in QML.

## Prerequisites

The application uses [Tomorrow.io](https://www.tomorrow.io/weather-api/) weather API, therefore you need an API key to access the calls. Create and put your API key in `text_files/apikey.txt` file - without this file the application will not function. Use QtCreator program to compile and run the application.


## Documentation    

The Doxygen documentation is available under the [address](https://emilia-szymanska.gitlab.io/meteoviz/index.html).

## Credits

Icons in `img` directory were taken from [free iconfinder](https://www.iconfinder.com/weather-icons?price=free) website.

## Layout

The layout of the application is presented below - there are 4 states of window presented. In the first window it is necessary to choose a location on the map or from the list, then the window is switched to the one with weather forecast and detailed data about current weather.

![all windows](img/all_windows.png)
